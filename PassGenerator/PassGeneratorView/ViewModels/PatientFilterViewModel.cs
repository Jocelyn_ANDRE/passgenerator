using System;
using System.Collections.Generic;
using System.Linq;
using PassGeneratorView.Models;

namespace PassGeneratorView.ViewModels;

public class PatientFilterViewModel
{
    private List<Patient> _allPatients = new List<Patient>();

    public void AddPatients(List<Patient> patients)
    {
        foreach (var patient in patients)
        {
            _allPatients.Add(patient);
        }
    }

    public List<Patient> GetFilteredPatient(string name, string firstName, DateTime? birthDate)
    {
        return _allPatients.Where(x =>
            x.Name.Contains(name) && x.FirstName.Contains(firstName) &&
            (birthDate == null || x.GetBirthDate == birthDate?.ToShortDateString())).ToList();
    }
}