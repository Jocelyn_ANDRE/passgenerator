using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using PassGeneratorView.Helpers;
using PassGeneratorView.Models;
using PassGeneratorView.ViewModels;
using PassGeneratorView.Enums;

namespace PassGeneratorView.ViewModels;

public class MainWindowViewModel : INotifyPropertyChanged
{
    private CancellationTokenSource _cancelToken;
    private object _lockCounter = new object();
    
    public MethodToValueConverter MethodToValueConverter = new MethodToValueConverter();
    public ICommand ReinitializeFiltersCommand { get; set; }
    public ICommand GeneratePatientsCommand { get; set; }
    public ICommand GeneratePassCommand { get; set; }
    public ICommand CancelPassCommand { get; set; }

    private BindingList<Patient> _patients = new();
    
    public BindingList<Patient> Patients
    {
        get => _patients;
        set { _patients = value;  OnPropertyChanged(nameof(Patients)); }
    }

    private string _name = string.Empty;
    
    public string NameFilter { get => _name;
        set { _name = value; UpdateListPatient(); OnPropertyChanged(nameof(NameFilter)); }
    }
    
    private string _firstName = string.Empty;
    
    public string FirstNameFilter { get => _firstName;
        set { _firstName = value; UpdateListPatient(); OnPropertyChanged(nameof(FirstNameFilter)); }
    }

    private DateTime? _birthDate;
    
    public DateTime? BirthDateFilter
    {
        get => _birthDate;
        set { _birthDate = value; UpdateListPatient(); OnPropertyChanged(nameof(BirthDateFilter)); }
    }

    private string _passGenerationStatus = ProgressBarStatusEnum.StandBy.GetDescription();
    
    public string PassGenerationStatus
    {
        get => _passGenerationStatus;
        set { _passGenerationStatus = value; OnPropertyChanged(nameof(PassGenerationStatus)); }
    }

    private int _progressBarValue;
    
    public int ProgressBarValue
    {
        get => _progressBarValue;
        set { _progressBarValue = value; OnPropertyChanged(nameof(ProgressBarValue)); }
    }

    private readonly PatientGeneratorViewModel PatientGeneratorViewModel = new PatientGeneratorViewModel();
    private readonly PassCreatorViewModel PassViewModel = new PassCreatorViewModel();
    private readonly PatientFilterViewModel PatientFilterViewModel = new PatientFilterViewModel();

    public MainWindowViewModel()
    {
        ReinitializeFiltersCommand = new RelayCommand(ReinitializeFilters);
        GeneratePatientsCommand = new RelayCommand(GeneratePatients);
        GeneratePassCommand = new RelayCommand(GeneratePass);
        CancelPassCommand = new RelayCommand(CancelPassGeneration);
    }

    private void GeneratePatients()
    {
        PatientFilterViewModel.AddPatients(PatientGeneratorViewModel.GenerateTenPatients());
        Patients = new BindingList<Patient>(
            PatientFilterViewModel.GetFilteredPatient(NameFilter, FirstNameFilter, BirthDateFilter));
    }

    private void GeneratePass()
    {
        var list = new List<string>();
        list.Add("1");
        list.Add("2");
        list.Add("3");
        list.Add("5");
        list.Add("1");
        list.Add("1");
        list.Add("1");
        list.Add("1");

        var list2 = new List<string>();
        list.Add("5");
        list.Add("6");
        list.Add("5");

        list2 = list;
        
        
        
        _cancelToken = new CancellationTokenSource();
        Task.Run(() => GeneratePassTask(DateTime.Now), _cancelToken.Token);
    }

    private void GeneratePassTask(DateTime dateTimeRequest)
    {
        PassGenerationStatus =  ProgressBarStatusEnum.Running.GetDescription();
        var listPatientTemp = Patients.Where(x => !x.HasPass).OrderBy(x => !x.StatusCovid);
        var countPatientToGeneratePass = listPatientTemp.Count();
        var counter = 0;
        ProgressBarValue = 0;
        
        foreach (var patient in listPatientTemp)
        {
            if (_cancelToken.IsCancellationRequested)
            {
                PassGenerationStatus =  ProgressBarStatusEnum.Canceled.GetDescription();
                return;
            }
            
            patient.RequestDateTime = dateTimeRequest;
            Thread.Sleep(1000);
            PassViewModel.GeneratePass(patient);
            
            if (patient.HasPass)
            {
                lock (_lockCounter)
                {
                    counter++;
                }
            }
            
            ProgressBarValue = (100 * counter) / countPatientToGeneratePass;
        }
        
        PassGenerationStatus =  ProgressBarStatusEnum.Completed.GetDescription();
    }

    private void CancelPassGeneration()
    {
        try
        {
            _cancelToken.Cancel();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private void UpdateListPatient()
    {
        Patients = new BindingList<Patient>(
            PatientFilterViewModel.GetFilteredPatient(NameFilter, FirstNameFilter, BirthDateFilter));
    }

    private void ReinitializeFilters()
    {
        NameFilter = string.Empty;
        FirstNameFilter = string.Empty;
        BirthDateFilter = null;
        UpdateListPatient();
    }
    
    public event PropertyChangedEventHandler? PropertyChanged;
    private void OnPropertyChanged(string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}