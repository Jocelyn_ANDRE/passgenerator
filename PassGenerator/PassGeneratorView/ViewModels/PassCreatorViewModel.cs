using System.Collections.Generic;
using PassGeneratorView.Models;

namespace PassGeneratorView.ViewModels;

public class PassCreatorViewModel
{
    public void GeneratePass(Patient patient)
    {
        patient.Pass = new Pass();
        patient.SetHasPass();
    }
}