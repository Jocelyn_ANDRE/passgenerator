using System;
using System.Collections.Generic;
using PassGeneratorView.Models;

namespace PassGeneratorView.ViewModels;

public class PatientGeneratorViewModel
{
    private readonly Random _random = new Random();
    
    private static readonly List<string> MostCommonFirstNames = new List<string>
    {
        "James", "Robert", "John", "Michael", "William", "David", "Richard", "Joseph", "Thomas", "Charles",
        "Christopher", "Daniel", "Matthew", "Anthony", "Mark",
        "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth", "Barbara", "Susan", "Jessica", "Sarah", "Karen",
        "Nancy", "Lisa", "Betty", "Margaret", "Sandra"
    };
    
    private static readonly List<string> MostCommonNames = new List<string>
    {
        "Smith", "Johnson", "Williams", "Brown", "Jones", "García", "Miller", "Davis", "Rodríguez", "Martínez",
        "Hernández", "Wilson", "Dupont", "Dubois", "Martin",
        "Maurin", "Adam", "Poirier", "Vidal", "Collin", "Aubry", "Cesar", "Henry", "Lambert", "Leroy",
        "Lemoine", "Jacob", "Renaud", "Deschamps", "Fleury"
    };
    
    
    public List<Patient> GenerateTenPatients()
    {
        var listNewPatients = new List<Patient>();
        var listFirstNamesCount = MostCommonFirstNames.Count;
        var listNamesCount = MostCommonNames.Count;
        
        var minimumBirthDate = new DateTime(1900, 1, 1);
        var range = (DateTime.Today - minimumBirthDate).Days;           

        for (var i = 0; i < 10; i++)
        {
            var firstName = MostCommonFirstNames[_random.Next(0, listFirstNamesCount)];
            var name = MostCommonNames[_random.Next(0, listNamesCount)];
            var birthDate = minimumBirthDate.AddDays(_random.Next(range)).Date;
            
            listNewPatients.Add(new Patient(name, firstName, birthDate, GetRandomCovidStatus()));
        }

        return listNewPatients;
    }

    private bool GetRandomCovidStatus()
    {
        return _random.Next(1, 5) <= 2;
    }
}