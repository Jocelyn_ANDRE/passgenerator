using System.ComponentModel;

namespace PassGeneratorView.Enums;

public enum ProgressBarStatusEnum
{
    [Description("Generation in standby")] StandBy,
    [Description("Generation in progress")] Running,
    [Description("Generation completed")] Completed,
    [Description("Generation canceled")] Canceled
}