using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PassGeneratorView.Helpers;

public class ProducerConsumerQueue
{
    private readonly ThreadPriority _threadPriority;
    private Thread _consumerThread;
    private CancellationToken _cancelToken;

    internal Queue<Action> Queue = new();

    public ProducerConsumerQueue(ThreadPriority priority)
    {
        _threadPriority = priority;
    }

    private void ExecuteQueue()
    {
        while (true)
        {
            while (Queue.TryDequeue(out Action action))
            {
                Task.Run(() => action(), _cancelToken);
            }
        }
    }

    public void AddAction(Action action)
    {
        Queue.Enqueue(action);
    }
    
    
}