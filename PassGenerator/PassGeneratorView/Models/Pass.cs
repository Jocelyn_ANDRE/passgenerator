using System;

namespace PassGeneratorView.Models;

public class Pass
{
    public Guid? PassCode { get; }

    public DateTime? RealisationDateTime { get; }

    public Pass()
    {
        PassCode = Guid.NewGuid();
        RealisationDateTime = DateTime.Now;
    }
}