using System;
using System.ComponentModel;
using System.Windows.Markup;

namespace PassGeneratorView.Models;

public class Patient: INotifyPropertyChanged
{
    public string Name { get; }
    public string FirstName { get; }

    private readonly DateTime _birthDate;
    
    public string GetBirthDate => _birthDate.ToShortDateString();
    public bool StatusCovid { get; set; }

    private DateTime? _requestDateTime;
    public DateTime? RequestDateTime
    {
        get => _requestDateTime; 
        set { 
            _requestDateTime = value;
            OnPropertyChanged(nameof(RequestDateTime));
        }
    }
    
    public Pass? Pass { get; set; }

    private bool _hasPass;

    public bool HasPass
    {
        get => _hasPass; 
        set
        {
            _hasPass = value;
            OnPropertyChanged(nameof(HasPass));
        }
    }

    public void SetHasPass()
    {
        HasPass = Pass != null;
    }

    public string GetPassDetails() => HasPass ? $"Pass id : {Pass?.PassCode} created on {Pass?.RealisationDateTime}" : "This patient has no pass";

    public Patient(string name, string firstName, DateTime birthDate, bool statusCovid)
    {
        Name = name;
        FirstName = firstName;
        _birthDate = birthDate;
        StatusCovid = statusCovid;
    }
    
    public event PropertyChangedEventHandler? PropertyChanged;
    protected void OnPropertyChanged(string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}